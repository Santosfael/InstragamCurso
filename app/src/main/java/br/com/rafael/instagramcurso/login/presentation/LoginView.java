package br.com.rafael.instagramcurso.login.presentation;

import br.com.rafael.instagramcurso.common.view.View;

public interface LoginView extends View {

    void onFailureForm(String emailError, String passwordError);

    void onUserLogged();
}
