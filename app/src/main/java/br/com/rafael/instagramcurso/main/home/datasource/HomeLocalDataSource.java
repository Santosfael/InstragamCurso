package br.com.rafael.instagramcurso.main.home.datasource;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.Database;
import br.com.rafael.instagramcurso.common.model.Feed;
import br.com.rafael.instagramcurso.common.presenter.Presenter;

public class HomeLocalDataSource implements HomeDataSource {
    @Override
    public void findFeed(Presenter<List<Feed>> presenter) {
        Database db = Database.getInstance();
        db.findFeed(db.getUser().getUserId())
                .addOnSuccessListener(presenter::onSuccess)
                .addOnFailureListener(e -> presenter.onError(e.getMessage()))
                .addOnCompleteListener(presenter::onComplete);
    }
}
