package br.com.rafael.instagramcurso.login.datasource;

import br.com.rafael.instagramcurso.common.presenter.Presenter;

public interface LoginDataSource {

    void login(String email, String password, Presenter presenter);
}
