package br.com.rafael.instagramcurso.main.camera.datasource;

import android.net.Uri;

import br.com.rafael.instagramcurso.common.model.Database;
import br.com.rafael.instagramcurso.common.presenter.Presenter;

public class AddLocalDataSource implements AddDataSource {
    @Override
    public void savePost(Uri uri, String caption, Presenter presenter) {
        Database db = Database.getInstance();
        db.createPost(db.getUser().getUserId(), uri, caption)
                .addOnSuccessListener((Database.OnSuccessListener<Void>) presenter::onSuccess)
                .addOnFailureListener(e -> presenter.onError(e.getMessage()))
                .addOnCompleteListener(presenter::onComplete);
    }
}
