package br.com.rafael.instagramcurso.main.search.presentation;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.view.AbstractFragment;
import br.com.rafael.instagramcurso.main.camera.presentation.GalleryFragment;
import br.com.rafael.instagramcurso.main.home.presentation.HomeFragment;
import br.com.rafael.instagramcurso.main.presentation.MainView;
import br.com.rafael.instragramcurso.R;
import butterknife.BindView;

public class SearchFragment extends AbstractFragment<SearchPresenter> implements MainView.SearchView {

    @BindView(R.id.search_recycler)
    RecyclerView recyclerView;

    private UserAdapter userAdapter;
    private MainView mainView;

    public static SearchFragment newInstance(MainView mainView, SearchPresenter presenter) {
        SearchFragment searchFragment = new SearchFragment();
        presenter.setView(searchFragment);
        searchFragment.setMainView(mainView);
        searchFragment.setPresenter(presenter);

        return searchFragment;
    }

    private void setMainView(MainView mainView) {
        this.mainView = mainView;
    }

    public SearchFragment() {}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        userAdapter = new UserAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(userAdapter);

        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main_search;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if(searchItem != null)
            searchView = (SearchView) searchItem.getActionView();

        if(searchView != null)
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(((AppCompatActivity) getContext()).getComponentName()));

        searchView.setOnQueryTextFocusChangeListener((v, hasFocus) -> Log.i("Teste", hasFocus + ""));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(!newText.isEmpty())
                    presenter.findUsers(newText);
                return false;
            }
        });
        searchItem.expandActionView();
    }

    @Override
    public void showUsers(List<User> users) {
        userAdapter.setUsers(users, user -> mainView.showProfile(user.getUuid()));
        userAdapter.notifyDataSetChanged();
    }

    public static class  PostViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imageUser;
        private final TextView textViewUsername;
        private final TextView textViewName;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            imageUser = itemView.findViewById(R.id.main_search_image_view_user);
            textViewUsername = itemView.findViewById(R.id.main_search_text_view_username);
            textViewName = itemView.findViewById(R.id.main_search_text_view_name);
        }

        public void bind(User user) {
            this.textViewUsername.setText(user.getName());
            this.imageUser.setImageURI(user.getUri());
            this.textViewName.setText(user.getName());

        }
    }

    interface ItemClickListener {
        void onClick(User user);
    }

    public class UserAdapter extends RecyclerView.Adapter<PostViewHolder> {

        private List<User> users = new ArrayList<>();
        private ItemClickListener listener;

        @NonNull
        @Override
        public PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = getLayoutInflater().inflate(R.layout.item_user_list, viewGroup, false);
            return new PostViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull PostViewHolder postViewHolder, int position) {
            postViewHolder.bind(users.get(position));
            postViewHolder.itemView.setOnClickListener(v -> {
                listener.onClick(users.get(position));
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        public void setUsers(List<User> users, ItemClickListener listener) {
            this.users = users;
            this.listener = listener;
        }
    }
}
