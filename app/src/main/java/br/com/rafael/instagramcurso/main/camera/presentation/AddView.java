package br.com.rafael.instagramcurso.main.camera.presentation;

import android.net.Uri;

public interface AddView {

    void onImageLoaded(Uri uri);
}
