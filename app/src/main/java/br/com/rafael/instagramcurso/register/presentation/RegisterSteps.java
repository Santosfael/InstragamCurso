package br.com.rafael.instagramcurso.register.presentation;

public enum RegisterSteps {

    EMAIL,
    NAME_PASSWORD,
    WELCOME,
    PHOTO;
}
