package br.com.rafael.instagramcurso.main.home.datasource;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.Feed;
import br.com.rafael.instagramcurso.common.presenter.Presenter;

public interface HomeDataSource {

    void findFeed(Presenter<List<Feed>> presenter);
}
