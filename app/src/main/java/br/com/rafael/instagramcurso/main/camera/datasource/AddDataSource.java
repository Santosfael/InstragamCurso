package br.com.rafael.instagramcurso.main.camera.datasource;

import android.net.Uri;

import br.com.rafael.instagramcurso.common.presenter.Presenter;

public interface AddDataSource {

    void savePost(Uri uri, String caption, Presenter presenter);

}
