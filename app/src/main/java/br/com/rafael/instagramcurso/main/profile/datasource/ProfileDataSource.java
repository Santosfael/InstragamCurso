package br.com.rafael.instagramcurso.main.profile.datasource;

import br.com.rafael.instagramcurso.common.model.UserProfile;
import br.com.rafael.instagramcurso.common.presenter.Presenter;

public interface ProfileDataSource {

    void findUser(String user, Presenter<UserProfile> presenter);

    void follow(String user);

    void unfollow(String user);
}
