package br.com.rafael.instagramcurso.common.view;

import android.content.Context;

public interface View {

    Context getContext();

    public void showProgressBar();

    public void hideProgressBar();

    void setStatusBarDark();
}
