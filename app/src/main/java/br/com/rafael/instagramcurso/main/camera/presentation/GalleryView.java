package br.com.rafael.instagramcurso.main.camera.presentation;

import android.net.Uri;

import java.util.List;

import br.com.rafael.instagramcurso.common.view.View;

public interface GalleryView extends View {

    void onPicturesLoaded(List<Uri> uris);
}
