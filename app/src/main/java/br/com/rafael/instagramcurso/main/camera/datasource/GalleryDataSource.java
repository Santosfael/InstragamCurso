package br.com.rafael.instagramcurso.main.camera.datasource;

import android.content.Context;

import br.com.rafael.instagramcurso.common.presenter.Presenter;

public interface GalleryDataSource {

    void findPictures(Context context, Presenter presenter);
}
