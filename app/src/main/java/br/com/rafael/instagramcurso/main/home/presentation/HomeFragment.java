package br.com.rafael.instagramcurso.main.home.presentation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.rafael.instagramcurso.common.model.Feed;
import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.view.AbstractFragment;
import br.com.rafael.instagramcurso.main.presentation.MainView;
import br.com.rafael.instragramcurso.R;
import butterknife.BindView;

public class HomeFragment extends AbstractFragment<HomePresenter> implements MainView.HomeView {

    @BindView(R.id.home_recycler)
    RecyclerView recyclerView;
    private FeedAdapter feedAdapter;
    private MainView mainView;

    public static HomeFragment newInstance(MainView mainView, HomePresenter homePresenter) {
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setMainView(mainView);
        homeFragment.setPresenter(homePresenter);
        homePresenter.setView(homeFragment);
        return homeFragment;
    }

    public HomeFragment() {}

    private void setMainView(MainView mainView) {
        this.mainView = mainView;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        feedAdapter = new FeedAdapter();
        RecyclerView recyclerView = view.findViewById(R.id.home_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(feedAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.findFeed();
    }

    @Override
    public void showProgressBar() {
        mainView.showProgressBar();
    }

    @Override
    public void hideProgressBar() {
        mainView.hideProgressBar();
    }

    @Override
    public void showFeed(List<Feed> response) {
        feedAdapter.setFeed(response);
        feedAdapter.notifyDataSetChanged();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main_home;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public class FeedAdapter extends RecyclerView.Adapter<PostViewHolder> {

        private List<Feed> feed = new ArrayList<>();

        public void setFeed(List<Feed> feed) {
            this.feed = feed;
        }

        @NonNull
        @Override
        public PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            // TODO: 09/03/2020 app:layout_scroolFlags = "scrool" na toolbar
            return new PostViewHolder(getLayoutInflater().inflate(R.layout.item_post_list,viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PostViewHolder postViewHolder, int i) {
            postViewHolder.bind(feed.get(i));
        }

        @Override
        public int getItemCount() {
            return feed.size();
        }
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imagePost;
        private final ImageView imageUser;
        private final TextView textViewCaption;
        private final TextView textViewUserName;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            imagePost = itemView.findViewById(R.id.profile_image_grid);
            imageUser = itemView.findViewById(R.id.home_container_user_photo);
            textViewUserName = itemView.findViewById(R.id.home_container_user_name);
            textViewCaption = itemView.findViewById(R.id.home_container_user_caption);
        }

        public void bind(Feed feed) {
            // TODO: 09/04/2020
            this.imagePost.setImageURI(feed.getUri());
            this.textViewCaption.setText(feed.getCaption());
            User user = feed.getPublisher();
            if(user != null) {
                this.imageUser.setImageURI(user.getUri());
                this.textViewUserName.setText(user.getName());
            }
        }
    }
}
