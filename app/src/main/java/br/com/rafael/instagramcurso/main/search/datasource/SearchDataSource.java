package br.com.rafael.instagramcurso.main.search.datasource;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.presenter.Presenter;

public interface SearchDataSource {

    void findUser(String query, Presenter<List<User>> presenter);
}
