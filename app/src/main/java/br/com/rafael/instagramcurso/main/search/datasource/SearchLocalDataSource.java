package br.com.rafael.instagramcurso.main.search.datasource;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.Database;
import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.presenter.Presenter;

public class SearchLocalDataSource implements SearchDataSource {

    @Override
    public void findUser(String query, Presenter<List<User>> presenter) {
        Database db = Database.getInstance();
        db.findUsers(db.getUser().getUserId(), query)
                .addOnSuccessListener(presenter::onSuccess)
                .addOnFailureListener(e -> presenter.onError(e.getMessage()))
                .addOnCompleteListener(presenter::onComplete);
    }
}
