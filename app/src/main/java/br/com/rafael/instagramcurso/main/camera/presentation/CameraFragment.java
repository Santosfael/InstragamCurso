package br.com.rafael.instagramcurso.main.camera.presentation;

import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import br.com.rafael.instagramcurso.common.component.CameraPreview;
import br.com.rafael.instagramcurso.common.component.MediaHelper;
import br.com.rafael.instagramcurso.common.view.AbstractFragment;
import br.com.rafael.instragramcurso.R;
import butterknife.BindView;
import butterknife.OnClick;

public class CameraFragment extends AbstractFragment {

    @BindView(R.id.camera_progress)
    ProgressBar progressBar;

    @BindView(R.id.camera_surface)
    FrameLayout frameLayout;

    @BindView(R.id.container_preview)
    LinearLayout containerPreview;

    @BindView(R.id.camera_image_view_picture)
    Button buttonCamera;

    private MediaHelper mediaHelper;
    private Camera camera;
    private AddView addView;

    public CameraFragment() {}

    public static CameraFragment newInstance(AddView addView) {
        CameraFragment cameraFragment = new CameraFragment();
        cameraFragment.setAddView(addView);
        return cameraFragment;
    }

    private void setAddView(AddView addView) {
        this.addView = addView;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if(getContext() != null) {
            mediaHelper = MediaHelper.getInstance(this);
            if(mediaHelper.checkCameraHardware(getContext())) {
                camera = mediaHelper.getCameraInstance();
                CameraPreview cameraPreview = new CameraPreview(getContext(), camera);
                frameLayout.addView(cameraPreview);
            }
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(camera != null)
            camera.release();
    }

    @OnClick(R.id.camera_image_view_picture)
    public void onCameraButtonClick() {
        progressBar.setVisibility(View.VISIBLE);
        buttonCamera.setVisibility(View.GONE);
        camera.takePicture(null, null, (data, camera) -> {
            progressBar.setVisibility(View.GONE);
            buttonCamera.setVisibility(View.VISIBLE);
            Uri uri = mediaHelper.saveCameFile(data);
            if(uri != null)
                addView.onImageLoaded(uri);
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main_camera;
    }
}
