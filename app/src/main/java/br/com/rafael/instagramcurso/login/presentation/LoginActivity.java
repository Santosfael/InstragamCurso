package br.com.rafael.instagramcurso.login.presentation;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import br.com.rafael.instagramcurso.common.model.Database;
import br.com.rafael.instagramcurso.common.model.UserAuth;
import br.com.rafael.instagramcurso.common.view.AbstractActivity;
import br.com.rafael.instagramcurso.common.component.LoadingButton;
import br.com.rafael.instagramcurso.login.datasource.LoginDataSource;
import br.com.rafael.instagramcurso.login.datasource.LoginLocalDataSource;
import br.com.rafael.instagramcurso.main.presentation.MainActivity;
import br.com.rafael.instagramcurso.register.presentation.RegisterActivity;
import br.com.rafael.instragramcurso.R;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity extends AbstractActivity implements LoginView {

    @BindView(R.id.login_edit_text_email) EditText editTextEmail;
    @BindView(R.id.login_edit_text_password) EditText editTextPassword;

    @BindView(R.id.login_edit_text_email_input) TextInputLayout inputLayoutTextEmail;
    @BindView(R.id.login_edit_text_password_input) TextInputLayout inputLayoutTextPassword;
    @BindView(R.id.login_button_enter) LoadingButton buttonEnter;

    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarDark();

        UserAuth user = Database.getInstance().getUser();
        if(user != null)
            onUserLogged();

    }

    @Override
    protected void onInject() {
        LoginDataSource dataSource = new LoginLocalDataSource();
        presenter = new LoginPresenter(this, dataSource);
    }

    @Override
    public void showProgressBar() {
        buttonEnter.showProgress(true);
    }

    @Override
    public void hideProgressBar() {
        buttonEnter.showProgress(false);
    }

    @Override
    public void onFailureForm(String emailError, String passwordError) {
        if(emailError != null) {
            inputLayoutTextEmail.setError(emailError);
            editTextEmail.setBackground(findDrawable(R.drawable.edit_text_background_error));
        }

        if(passwordError != null) {
            inputLayoutTextPassword.setError(passwordError);
            editTextPassword.setBackground(findDrawable(R.drawable.edit_text_background_error));
        }
    }

    @Override
    public void onUserLogged() {
        MainActivity.launch(this, MainActivity.LOGIN_ACTIVITY);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @OnClick(R.id.login_button_enter)
    public void onButtonEnterClick() {
        presenter.login(editTextEmail.getText().toString(), editTextPassword.getText().toString());
    }

    @OnClick(R.id.login_text_view_register)
    public void onTextViewRegisterClick() {
        RegisterActivity.launch(this);
    }

    @OnTextChanged({R.id.login_edit_text_email, R.id.login_edit_text_password})
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        buttonEnter.setEnabled(
                !editTextEmail.getText().toString().isEmpty() &&
                        !editTextPassword.getText().toString().isEmpty()
        );

        if(s.hashCode() == editTextEmail.getText().hashCode()) {
            editTextEmail.setBackground(findDrawable(R.drawable.edit_text_background));
            inputLayoutTextEmail.setError(null);
            inputLayoutTextEmail.setErrorEnabled(false);
        }else if(s.hashCode() == editTextPassword.getText().hashCode()) {
            editTextPassword.setBackground(findDrawable(R.drawable.edit_text_background));
            inputLayoutTextPassword.setError(null);
            inputLayoutTextPassword.setErrorEnabled(false);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }
}
