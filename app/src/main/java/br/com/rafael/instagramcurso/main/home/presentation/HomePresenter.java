package br.com.rafael.instagramcurso.main.home.presentation;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.Feed;
import br.com.rafael.instagramcurso.common.presenter.Presenter;
import br.com.rafael.instagramcurso.main.home.datasource.HomeDataSource;
import br.com.rafael.instagramcurso.main.presentation.MainView;

public class HomePresenter implements Presenter<List<Feed>> {

    private final HomeDataSource dataSource;
    private MainView.HomeView view;

    public HomePresenter(HomeDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setView(MainView.HomeView view) {
        this.view = view;
    }

    public void findFeed() {
        view.showProgressBar();
        dataSource.findFeed(this);
    }

    @Override
    public void onSuccess(List<Feed> response) {
        view.showFeed(response);
    }

    @Override
    public void onError(String message) {
        // TODO: 09/04/2020
    }

    @Override
    public void onComplete() {
        view.hideProgressBar();
    }
}
