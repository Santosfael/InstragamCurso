package br.com.rafael.instagramcurso.login.presentation;


import br.com.rafael.instagramcurso.common.model.UserAuth;
import br.com.rafael.instagramcurso.common.presenter.Presenter;
import br.com.rafael.instagramcurso.common.util.Strings;
import br.com.rafael.instagramcurso.login.datasource.LoginDataSource;
import br.com.rafael.instragramcurso.R;

class LoginPresenter implements Presenter<UserAuth> {

    private final LoginView view;
    private final LoginDataSource dataSource;

    LoginPresenter(LoginView view, LoginDataSource dataSource) {
        this.view = view;
        this.dataSource = dataSource;
    }

    void login(String email, String password) {
        if(!Strings.emailValid(email)) {
            view.onFailureForm(view.getContext().getString(R.string.invalid_email), null);
            return;
        }
        view.showProgressBar();
        dataSource.login(email, password, this);
    }

    @Override
    public void onSuccess(UserAuth userAuth) {
        view.onUserLogged();
    }

    @Override
    public void onError(String message) {
        view.onFailureForm(null, message);
    }

    @Override
    public void onComplete() {
        view.hideProgressBar();
    }
}
