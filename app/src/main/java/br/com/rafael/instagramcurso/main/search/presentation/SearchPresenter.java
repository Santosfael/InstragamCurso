package br.com.rafael.instagramcurso.main.search.presentation;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.presenter.Presenter;
import br.com.rafael.instagramcurso.main.presentation.MainView;
import br.com.rafael.instagramcurso.main.search.datasource.SearchDataSource;

public class SearchPresenter implements Presenter<List<User>> {

    private final SearchDataSource dataSource;
    private MainView.SearchView view;

    public SearchPresenter(SearchDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setView(MainView.SearchView view) {
        this.view = view;
    }

    public void findUsers(String newText) {
        dataSource.findUser(newText, this);
    }

    @Override
    public void onSuccess(List<User> response) {
        view.showUsers(response);
    }

    @Override
    public void onError(String message) {
        // TODO: 14/04/2020
    }

    @Override
    public void onComplete() {
    }

}
