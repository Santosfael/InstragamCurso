package br.com.rafael.instagramcurso.main.camera.presentation;

import android.net.Uri;

import br.com.rafael.instagramcurso.common.presenter.Presenter;
import br.com.rafael.instagramcurso.main.camera.datasource.AddDataSource;

public class AddPresenter implements Presenter<Void> {

    private final AddCaptionView view;
    private final AddDataSource dataSource;

    public AddPresenter(AddCaptionView view, AddDataSource dataSource) {
        this.view = view;
        this.dataSource = dataSource;
    }

    void createPost(Uri uri, String caption) {
        view.showProgressBar();
        dataSource.savePost(uri, caption, this);
    }

    @Override
    public void onSuccess(Void response) {
        view.postSaved();
    }

    @Override
    public void onError(String message) {
        // TODO: 13/04/2020
    }

    @Override
    public void onComplete() {
        view.hideProgressBar();
    }
}
