package br.com.rafael.instagramcurso.common.presenter;

import br.com.rafael.instagramcurso.common.model.UserAuth;

public interface Presenter<T> {

    void onSuccess(T response);
    void onError(String message);
    void onComplete();
}
