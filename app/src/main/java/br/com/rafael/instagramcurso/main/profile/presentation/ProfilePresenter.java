package br.com.rafael.instagramcurso.main.profile.presentation;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.Database;
import br.com.rafael.instagramcurso.common.model.Post;
import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.model.UserProfile;
import br.com.rafael.instagramcurso.common.presenter.Presenter;
import br.com.rafael.instagramcurso.main.presentation.MainView;
import br.com.rafael.instagramcurso.main.profile.datasource.ProfileDataSource;

public class ProfilePresenter implements Presenter<UserProfile> {

    private final ProfileDataSource dataSource;
    private final String user;
    private MainView.ProfileView view;

    public ProfilePresenter(ProfileDataSource dataSource) {
        this(dataSource, Database.getInstance().getUser().getUserId());
    }

    public ProfilePresenter(ProfileDataSource dataSource, String user) {
        this.dataSource = dataSource;
        this.user = user;
    }

    public void setView(MainView.ProfileView view) {
        this.view = view;
    }

    public String getUser() {
        return user;
    }

    public void findUser() {
        view.showProgressBar();
        dataSource.findUser(user,this);
    }

    public void follow(boolean follow) {
        if(follow)
            dataSource.follow(user);
        else
            dataSource.unfollow(user);
    }

    @Override
    public void onSuccess(UserProfile userProfile) {
        User user = userProfile.getUser();
        List<Post> posts = userProfile.getPosts();

        boolean editProfile = user.getUuid().equals(Database.getInstance().getUser().getUserId());

        view.showData(
                user.getName(),
                String.valueOf(user.getFollowing()),
                String.valueOf(user.getFollowers()),
                String.valueOf(user.getPosts()),
                editProfile,
                userProfile.isFollowing()
        );

        view.showPost(posts);

        if(user.getUri() != null)
            view.showPhoto(user.getUri());
    }

    @Override
    public void onError(String message) {
        // TODO: 03/04/2020
    }

    @Override
    public void onComplete() {
        view.hideProgressBar();
    }
}
