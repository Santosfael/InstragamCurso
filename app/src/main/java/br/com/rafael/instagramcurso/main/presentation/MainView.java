package br.com.rafael.instagramcurso.main.presentation;

import android.net.Uri;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.Feed;
import br.com.rafael.instagramcurso.common.model.Post;
import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.view.View;

public interface MainView extends View {

    void scrollToolbarEnabled(boolean enabled);

    void showProfile(String user);

    void disposeProfileDetail();

    public interface ProfileView extends View {

        void showPhoto(Uri photo);

        void showData(String name, String following, String followers, String posts, boolean editProfile, boolean follow);

        void showPost(List<Post> posts);
    }

    public interface HomeView extends View {
        void showFeed(List<Feed> response);
    }

    public interface SearchView {
        void showUsers(List<User> users);
    }
}
