package br.com.rafael.instagramcurso.main.presentation;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import br.com.rafael.instagramcurso.common.model.Database;
import br.com.rafael.instagramcurso.main.camera.presentation.AddActivity;
import br.com.rafael.instagramcurso.common.view.AbstractActivity;
import br.com.rafael.instagramcurso.main.home.datasource.HomeDataSource;
import br.com.rafael.instagramcurso.main.home.datasource.HomeLocalDataSource;
import br.com.rafael.instagramcurso.main.home.presentation.HomeFragment;
import br.com.rafael.instagramcurso.main.home.presentation.HomePresenter;
import br.com.rafael.instagramcurso.main.profile.datasource.ProfileDataSource;
import br.com.rafael.instagramcurso.main.profile.datasource.ProfileLocalDataSource;
import br.com.rafael.instagramcurso.main.profile.presentation.ProfileFragment;
import br.com.rafael.instagramcurso.main.profile.presentation.ProfilePresenter;
import br.com.rafael.instagramcurso.main.search.datasource.SearchDataSource;
import br.com.rafael.instagramcurso.main.search.datasource.SearchLocalDataSource;
import br.com.rafael.instagramcurso.main.search.presentation.SearchFragment;
import br.com.rafael.instagramcurso.main.search.presentation.SearchPresenter;
import br.com.rafael.instragramcurso.R;

public class MainActivity extends AbstractActivity implements BottomNavigationView.OnNavigationItemSelectedListener, MainView {

    public static final int LOGIN_ACTIVITY = 0;
    public static final int REGISTER_ACTIVITY = 1;
    public static final String ACT_SOURCE = "act_source";

    private ProfilePresenter profilePresenter;
    private HomePresenter homePresenter;
    private SearchPresenter searchPresenter;

    Fragment homeFragment;
    Fragment profileFragment;
    //Fragment cameraFragment;
    Fragment searchFragment;
    Fragment active;

    ProfileFragment profileDetailFragment;

    public static void launch(Context context, int source) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(ACT_SOURCE, source);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null) {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_insta_camera);
            getSupportActionBar().setHomeAsUpIndicator(drawable);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onInject() {
        ProfileDataSource profileDataSource = new ProfileLocalDataSource();
        HomeDataSource homeDataSource = new HomeLocalDataSource();

        profilePresenter = new ProfilePresenter(profileDataSource);
        homePresenter = new HomePresenter(homeDataSource);

        homeFragment = HomeFragment.newInstance(this, homePresenter);
        profileFragment = ProfileFragment.newInstance(this, profilePresenter);

        SearchDataSource searchDataSource = new SearchLocalDataSource();
        searchPresenter = new SearchPresenter(searchDataSource);

        //cameraFragment = new CameraFragment();
        searchFragment = SearchFragment.newInstance(this, searchPresenter);

        active = homeFragment;

        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.main_fragment, profileFragment).hide(profileFragment).commit();
        ///fm.beginTransaction().add(R.id.main_fragment, cameraFragment).hide(cameraFragment).commit();
        fm.beginTransaction().add(R.id.main_fragment, searchFragment).hide(searchFragment).commit();
        fm.beginTransaction().add(R.id.main_fragment, homeFragment).hide(homeFragment).commit();
    }

    @Override
    public void showProgressBar() {
        findViewById(R.id.main_progress).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        findViewById(R.id.main_progress).setVisibility(View.GONE);
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        BottomNavigationView bv = findViewById(R.id.main_bottom_nav);
        bv.setOnNavigationItemSelectedListener(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            int source = extras.getInt(ACT_SOURCE);
            if(source == REGISTER_ACTIVITY) {
                getSupportFragmentManager().beginTransaction().hide(active).show(profileFragment).commit();
                active = profileFragment;
                scrollToolbarEnabled(true);
                profilePresenter.findUser();
            }
        }
    }


    @Override
    public void scrollToolbarEnabled(boolean enabled) {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        AppBarLayout appBarLayout = findViewById(R.id.main_app_bar);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        CoordinatorLayout.LayoutParams appBarLayoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();

        if(enabled) {
            params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
            appBarLayoutParams.setBehavior(new AppBarLayout.Behavior());
            appBarLayout.setLayoutParams(appBarLayoutParams);
        } else {
            params.setScrollFlags(0);
            appBarLayoutParams.setBehavior(null);
            appBarLayout.setLayoutParams(appBarLayoutParams);
        }
    }

    @Override
    public void showProfile(String user) {
        ProfileDataSource profileDataSource = new ProfileLocalDataSource();
        ProfilePresenter profilePresenter = new ProfilePresenter(profileDataSource, user);

        profileDetailFragment = ProfileFragment.newInstance(this, profilePresenter);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_fragment, profileDetailFragment, "detail");
        transaction.hide(active);
        transaction.commit();

        scrollToolbarEnabled(true);

        if(getSupportActionBar() != null) {
            Drawable drawable = findDrawable(R.drawable.ic_arrow_back);
            getSupportActionBar().setHomeAsUpIndicator(drawable);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void disposeProfileDetail() {
        if(getSupportActionBar() != null) {
            Drawable drawable = findDrawable(R.drawable.ic_insta_camera);
            getSupportActionBar().setHomeAsUpIndicator(drawable);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(profileDetailFragment);
        transaction.show(active);
        transaction.commit();

        profileDetailFragment = null;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentManager fm = getSupportFragmentManager();
        switch (menuItem.getItemId()) {
            case R.id.menu_bottom_home:
                if(profileDetailFragment != null)
                    disposeProfileDetail();

                fm.beginTransaction().hide(active).show(homeFragment).commit();
                active = homeFragment;
                //homePresenter.findFeed();
                scrollToolbarEnabled(false);
                return true;

            case R.id.menu_bottom_search:
                if(profileDetailFragment == null) {
                    fm.beginTransaction().hide(active).show(searchFragment).commit();
                    active = searchFragment;
                    scrollToolbarEnabled(false);
                }
                return true;

            case R.id.menu_bottom_add:
                //fm.beginTransaction().hide(active).show(cameraFragment).commit();
                //active = cameraFragment;
                AddActivity.launch(this);
                return true;

            case R.id.menu_bottom_profile:
                if(profileDetailFragment != null)
                    disposeProfileDetail();

                fm.beginTransaction().hide(active).show(profileFragment).commit();
                active = profileFragment;
                profilePresenter.findUser();
                scrollToolbarEnabled(true);
                return true;
        }
        return false;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }
}
