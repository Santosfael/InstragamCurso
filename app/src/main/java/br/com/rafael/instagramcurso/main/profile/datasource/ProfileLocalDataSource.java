package br.com.rafael.instagramcurso.main.profile.datasource;

import java.util.List;

import br.com.rafael.instagramcurso.common.model.Database;
import br.com.rafael.instagramcurso.common.model.Post;
import br.com.rafael.instagramcurso.common.model.User;
import br.com.rafael.instagramcurso.common.model.UserProfile;
import br.com.rafael.instagramcurso.common.presenter.Presenter;

public class ProfileLocalDataSource implements ProfileDataSource {
    @Override
    public void findUser(String uuid, Presenter<UserProfile> presenter) {
        Database db = Database.getInstance();
        db.findUser(uuid)
            .addOnSuccessListener((Database.OnSuccessListener<User>) user1 -> {
                db.findPosts(uuid)
                    .addOnSuccessListener((Database.OnSuccessListener<List<Post>>) posts -> {
                        db.following(db.getUser().getUserId(), uuid)
                                .addOnSuccessListener((Database.OnSuccessListener<Boolean>) following -> {
                                    presenter.onSuccess(new UserProfile(user1, posts, following));
                                    presenter.onComplete();
                                });

                    });
            });
    }

    @Override
    public void follow(String user) {
        Database.getInstance().follow(Database.getInstance().getUser().getUserId(), user);
    }

    @Override
    public void unfollow(String user) {
        Database.getInstance().unfollow(Database.getInstance().getUser().getUserId(), user);
    }
}
